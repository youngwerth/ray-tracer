import Foundation

struct Sphere: Hitable {
  let center: Vec3
  let radius: Double
  let material: Material

  func hit(ray: Ray, tMin: Double, tMax: Double) -> HitRecord? {
    let oc = ray.origin - center
    let a = dot(ray.direction, ray.direction)
    let b = dot(oc, ray.direction)
    let c = dot(oc, oc) - radius * radius
    let discriminant = b * b - a * c
    if discriminant > 0 {
      // TODO: Decrease redundancies
      var temp = (-b - sqrt(discriminant)) / a
      if temp < tMax && temp > tMin {
        let point = ray.point(at: temp)
        return HitRecord(
          t: temp,
          p: point,
          normal: (point - center) / radius,
          material: material
        )
      }

      temp = (-b + sqrt(discriminant)) / a
      if temp < tMax && temp > tMin {
        let point = ray.point(at: temp)
        return HitRecord(
          t: temp,
          p: point,
          normal: (point - center) / radius,
          material: material
        )
      }
    }

    return nil
  }
}

struct MovingSphere: Hitable {
  let center0, center1: Vec3
  let time0, time1: Double
  let radius: Double
  let material: Material

  func center(_ time: Double) -> Vec3 {
    return center0 + ((time - time0) / (time1 - time0)) * (center1 - center0)
  }

  func hit(ray: Ray, tMin: Double, tMax: Double) -> HitRecord? {
    let oc = ray.origin - center(ray.time)
    let a = dot(ray.direction, ray.direction)
    let b = dot(oc, ray.direction)
    let c = dot(oc, oc) - radius * radius
    let discriminant = b * b - a * c
    if discriminant > 0 {
      // TODO: Decrease redundancies
      var temp = (-b - sqrt(discriminant)) / a
      if temp < tMax && temp > tMin {
        let point = ray.point(at: temp)
        return HitRecord(
          t: temp,
          p: point,
          normal: (point - center(ray.time)) / radius,
          material: material
        )
      }

      temp = (-b + sqrt(discriminant)) / a
      if temp < tMax && temp > tMin {
        let point = ray.point(at: temp)
        return HitRecord(
          t: temp,
          p: point,
          normal: (point - center(ray.time)) / radius,
          material: material
        )
      }
    }

    return nil
  }
}