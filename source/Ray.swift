struct Ray {
  let origin, direction: Vec3
  let time: Double

  init(origin: Vec3, direction: Vec3, time: Double = 0.0) {
    self.origin = origin
    self.direction = direction
    self.time = time
  }

  func point(at t: Double) -> Vec3 {
    return origin + t * direction
  }
}

extension Ray {
  init(_ a: Vec3, _ b: Vec3, _ time: Double = 0.0) {
    origin = a; direction = b;
    self.time = time
  }
}
