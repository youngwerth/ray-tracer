import Foundation

func write(text: String, to: String) {
  do {
    try text.write(toFile: to, atomically: false, encoding: .utf8)
  } catch {
    print(error)
  }
}

func dot(_ v1: Vec3, _ v2: Vec3) -> Double {
  return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z)
}

func cross(_ v1: Vec3, _ v2: Vec3) -> Vec3 {
  return Vec3(
    x: (v1.y * v2.z) - (v1.z * v2.y),
    y: -((v1.x * v2.z) - (v1.z * v2.x)),
    z: (v1.x * v2.y) - (v1.y * v2.x)
  )
}

func unitVector(_ v: Vec3) -> Vec3 {
  return v / v.length
}

func randomInUnitSphere() -> Vec3 {
  var p: Vec3
  repeat {
    p = 2.0 * Vec3(rnd(), rnd(), rnd()) - Vec3(1.0, 1.0, 1.0)
  } while p.squaredLength > 1.0
  return p
}

func reflect(vec: Vec3, normal: Vec3) -> Vec3 {
  return vec - 2 * dot(vec, normal) * normal
}

func schlick(cosine: Double, ri: Double) -> Double {
  var r0 = (1 - ri) / (1 + ri)
  r0 *= r0
  return r0 + (1 - r0) * pow((1 - cosine), 5)
}

func refract(vec: Vec3, normal: Vec3, niOverNt: Double) -> Vec3? {
  let uv = unitVector(vec)
  let dt: Double = dot(uv, normal)
  let discriminant: Double = 1.0 - niOverNt * niOverNt * (1 - dt * dt)
  if discriminant > 0 {
    return niOverNt * (uv - normal * dt) - normal * sqrt(discriminant)
  }

  return nil
}

func randomScene() -> Hitable {
  var list = [Hitable]()

  list.append(Sphere(center: Vec3(0, -1000, 0), radius: 1000, material: Lambertian(albedo: Vec3(0.5, 0.5, 0.5))))

  for a in -11..<11 {
    for b in -11..<11 {
      let chooseMat = rnd()
      let center = Vec3(Double(a) + 0.9 * rnd(), 0.2, Double(b) + 0.9 * rnd())
      if (center - Vec3(4, 0.2, 0)).length > 0.9 {
        if chooseMat < 0.8 {
          list.append(MovingSphere(
            center0: center, center1: center + Vec3(0, 0.5 * rnd(), 0),
            time0: 0.0, time1: 1.0, radius: 0.2, 
            material: Lambertian(albedo: Vec3(rnd() * rnd(), rnd() * rnd(), rnd() * rnd())) 
          ))
          continue
        }

        if chooseMat < 0.95 {
          list.append(Sphere(
            center: center, radius: 0.2,
            material: Metal(
              albedo: Vec3(0.5 * (1 + rnd()), 0.5 * (1 + rnd()), 0.5 * (1 + rnd())),
              fuzz: 0.5 * rnd()
            )
          ))
          continue
        }

        list.append(Sphere(center: center, radius: 0.2, material: Dielectric(ri: 1.5)))
      }
    }
  }

  list.append(Sphere(
    center: Vec3(0, 1, 0), radius: 1.0,
    material: Dielectric(ri: 1.5)
  ))
  list.append(Sphere(
    center: Vec3(-4, 1, 0), radius: 1.0,
    material: Lambertian(albedo: Vec3(0.1, 0.2, 0.5))
  ))
  list.append(Sphere(
    center: Vec3(4, 1, 0), radius: 1.0,
    material: Metal(albedo: Vec3(0.7, 0.6, 0.5), fuzz: 0.0)
  ))

  return HitableList(list: list)
}