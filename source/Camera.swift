import Foundation

func randomInUnitDisk() -> Vec3 {
  var p: Vec3
  repeat {
    p = 2.0 * Vec3(rnd(), rnd(), 0) - Vec3(1, 1, 0)
  } while dot(p, p) > 1.0
  return p
}

struct Camera {
  let lowerLeftCorner: Vec3
  let horizontal: Vec3
  let vertical: Vec3
  let origin: Vec3
  let w, u, v: Vec3
  let lensRadius: Double
  let time0, time1: Double

  init(
    from: Vec3, to: Vec3, vup: Vec3, vfov: Double, aspect: Double,
    aperture: Double, focus: Double, time0: Double = 0.0, time1: Double = 0.0
  ) {
    self.time0 = time0
    self.time1 = time1
    lensRadius = aperture / 2.0
    let theta = vfov * Double.pi / 180.0
    let halfHeight = tan(theta / 2)
    let halfWidth = aspect * halfHeight
    w = unitVector(from - to)
    u = unitVector(cross(vup, w))
    v = cross(w, u)
    origin = from
    lowerLeftCorner = origin - halfWidth * u * focus - halfHeight * v * focus - w * focus
    horizontal = 2 * halfWidth * u * focus
    vertical = 2 * halfHeight * v * focus
  }

  func getRay(s: Double, t: Double) -> Ray {
    let rd: Vec3 = lensRadius * randomInUnitDisk()
    let offset = u * rd.x + v * rd.y
    let time = time0 + rnd() * (time1 - time0)
    return Ray(
      origin: origin + offset,
      direction: lowerLeftCorner + s * horizontal + t * vertical - origin - offset,
      time: time
    )
  }
}
