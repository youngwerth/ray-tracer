import Foundation

func color(ray: Ray, world: Hitable, depth: Int) -> Vec3 {
  let couldHit = world.hit(ray: ray, tMin: 0.001, tMax: Double.greatestFiniteMagnitude)
  if let didHit = couldHit {
    let data = didHit.material.scatter(rIn: ray, rec: didHit)
    if depth < 50 && data != nil {
      let (attenuation: atten, scattered: sct) = data!
      return atten * color(ray: sct, world: world, depth: depth + 1)
    }

    return Vec3(0.0, 0.0, 0.0)
  }

  let ud = unitVector(ray.direction)
  let t = 0.5 * (ud.y + 1.0)
  return (1.0 - t) * Vec3(1.0, 1.0, 1.0) + t * Vec3(0.5, 0.7, 1.0)
}

func render() {
  let nx = 1200
  let ny = 800
  let ns = 10
  let from = Vec3(13, 2, 3)
  let to = Vec3(0, 0, 0)
  let cam = Camera(
    from: from,
    to: to,
    vup: Vec3(0, 1, 0),
    vfov: 20,
    aspect: Double(nx) / Double(ny),
    aperture: 0.0,
    focus: 10,
    time0: 0.0,
    time1: 1.0
  )

  let world = randomScene()

  var file: String = "P3\n\(nx) \(ny)\n255\n"
  for j in (0..<ny).reversed() {
    for i in 0..<nx {
      var col = Vec3(0.0, 0.0, 0.0)
      for _ in 0..<ns {
        let u = (Double(i) + rnd()) / Double(nx)
        let v = (Double(j) + rnd()) / Double(ny)
        let ray = cam.getRay(s: u, t: v)
        col += color(ray: ray, world: world, depth: 0)
      }
      col /= Double(ns)
      col = Vec3(sqrt(col.x), sqrt(col.y), sqrt(col.z))
      let ir = Int(255.99 * col.r)
      let ig = Int(255.99 * col.g)
      let ib = Int(255.99 * col.b)
      file += "\(ir) \(ig) \(ib)\n"
    }
  }

  write(text: file, to: "render.ppm")
}

render()