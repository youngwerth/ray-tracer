#if os(Linux)
import Glibc
#else
import Darwin.C
#endif

#if os(Linux)
let LINUX_MAX = Double(RAND_MAX)
#else
let DARWIN_MAX = Double(UINT32_MAX)
#endif

func rnd() -> Double {
#if os(Linux)
  return Double(random()) / LINUX_MAX
#else
  return Double(arc4random()) / DARWIN_MAX
#endif
}