struct HitRecord {
  let t: Double
  let p: Vec3
  let normal: Vec3
  let material: Material
}

protocol Hitable {
  func hit(ray: Ray, tMin: Double, tMax: Double) -> HitRecord?
}

struct HitableList: Hitable {
  let list: [Hitable]

  func hit(ray: Ray, tMin: Double, tMax: Double) -> HitRecord? {
    var finalRecord: HitRecord?
    var closest: Double = tMax
    for hitable in list {
      if let record = hitable.hit(ray: ray, tMin: tMin, tMax: closest) {
        closest = record.t
        finalRecord = record
      }
    }

    return finalRecord
  }
}