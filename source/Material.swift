protocol Material {
  func scatter(rIn: Ray, rec: HitRecord)
    -> (attenuation: Vec3, scattered: Ray)?
}

struct Lambertian: Material {
  let albedo: Vec3

  func scatter(rIn: Ray, rec: HitRecord)
      -> (attenuation: Vec3, scattered: Ray)? {

    let target: Vec3 = rec.p + rec.normal + randomInUnitSphere()
    return (
      scattered: Ray(origin: rec.p, direction: target - rec.p, time: rIn.time),
      attenuation: albedo
    )
  }
}

struct Metal: Material {
  let albedo: Vec3
  let fuzz: Double

  init(albedo: Vec3, fuzz: Double) {
    self.albedo = albedo
    self.fuzz = fuzz > 1.0 ? 1.0 : fuzz
  }

  func scatter(rIn: Ray, rec: HitRecord)
      -> (attenuation: Vec3, scattered: Ray)? {

    let reflected = reflect(vec: unitVector(rIn.direction), normal: rec.normal)
    let scattered = Ray(rec.p, reflected + fuzz * randomInUnitSphere(), rIn.time)
    if dot(scattered.direction, rec.normal) > 0 {
      return (attenuation: albedo, scattered: scattered)
    }

    return nil
  }
}

struct Dielectric: Material {
  let ri: Double

  func scatter(rIn: Ray, rec: HitRecord)
      -> (attenuation: Vec3, scattered: Ray)? {
        
    let outwardNormal: Vec3
    let niOverNt: Double
    let reflectProb: Double
    let cosine: Double
    let attenuation = Vec3(1.0, 1.0, 1.0)
    let reflected = reflect(vec: rIn.direction, normal: rec.normal)
    if (dot(rIn.direction, rec.normal) > 0) {
      outwardNormal = -rec.normal
      niOverNt = ri
      cosine = ri * dot(rIn.direction, rec.normal) / rIn.direction.length
    } else {
      outwardNormal = rec.normal
      niOverNt = 1.0 / ri
      cosine = -dot(rIn.direction, rec.normal) / rIn.direction.length
    }

    let refracted = refract(
      vec: rIn.direction, normal: outwardNormal, niOverNt: niOverNt
    ) 
    
    if refracted != nil {
      reflectProb = schlick(cosine: cosine, ri: ri)
    } else {
      return (attenuation: attenuation, scattered: Ray(
        origin: rec.p, direction: reflected, time: rIn.time
      ))
    }

    if (rnd() < reflectProb) {
      return (attenuation: attenuation, scattered: Ray(
        origin: rec.p, direction: reflected, time: rIn.time
      ))
    }

    return (attenuation: attenuation, scattered: Ray(
      origin: rec.p, direction: refracted!
    ))
  }
}